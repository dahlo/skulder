```
Usage: python skulder.py -i <infile> [-m <multiplicator to convert currency> <-f for fance output format>]

Expected formatting of input file is 4 columns:
amount	by	for reason

Ex.

40	m	a,j,g,m	dinner

which means m payed for dinner, and a, j, g, and m should split the debt equally.


Ex.

40	m,a	a,j,g,m	lunch

which means m and a payed equally much for the lunch, and a, j, g and m should split the debt equally.


Options:
  -h, --help            show this help message and exit
  -i INFILE, --input=INFILE
                        The file that contains the debts.
  -m MULTIPLICATOR, --multiplicator=MULTIPLICATOR
                        If the debts are in a foregin currency it can be
                        converted to another currency before presenting the
                        results. The numbers in the debts file will all be
                        multiplied by this factor.
  -f, --fancy           Will produce a html file with a fancy presentation of
                        the debts if specified.
```