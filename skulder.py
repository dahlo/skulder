#!/usr/bin/pyton


# load libraries
import sys
import os
import re
import string
from optparse import OptionParser








### Define functions

# adjust balance of person with sum
def adjBalance(person, sum):

	# get the balance dict
	global balance


	try:
		balance[person] += sum
	except KeyError:
		balance[person] = sum


#fancy output function
def fancy(balance):

	1+1









# define usage message
helpMsg = """\nUsage: python %s -i <infile> [-m <multiplicator to convert currency> <-f for fance output format>]

Expected formatting of input file is 4 columns:
amount	by	for reason

Ex.

40	m	a,j,g,m	dinner

which means m payed for dinner, and a, j, g, and m should split the debt equally.


Ex.

40	m,a	a,j,g,m	lunch

which means m and a payed equally much for the lunch, and a, j, g and m should split the debt equally.\n""" % sys.argv[0]


# parse the options
parser = OptionParser(usage=helpMsg)
parser.add_option("-i", "--input", action="store", type="string", dest="infile", help="The file that contains the debts.")
parser.add_option("-m", "--multiplicator", action="store", type="float", dest="multiplicator", help="If the debts are in a foregin currency it can be converted to another currency before presenting the results. The numbers in the debts file will all be multiplied by this factor.", default=1)
parser.add_option("-f", "--fancy", action="store_true", dest="fancy", default=True, help="Will produce a html file with a fancy presentation of the debts if specified.")
(options, args) = parser.parse_args()

# check if the required options are there
if not options.infile:
	sys.exit("ERROR: -i not specified.")







# open file
file = open(options.infile,'r')

# init
balance = dict()



# for all entrys
for line in file:

	# skip commented lines or empty
	match = re.search('^(#.*)?$', line)
	if match:
		continue # skipt to next loop

	# search the line
	match = re.search('^(\S+)\s+(\S+)\s+(\S+)\s+(.+)$', line.rstrip())

	# if a valid line is found
	if match:

		# convert the numbers into numbers
		amount = float(match.groups()[0])

		# get payers and split the into a list
		payers = match.groups()[1].split(',')

		# get loaners and split them into a lists
		loaners = match.groups()[2].split(',')

		# get reason    
		reason = match.groups()[3]


	# notify if there is a strange line
	else:
		print "Not a valid line: %s" % line.rstrip()



	### do the magic

	# for each payer
	for payer in payers:
		# print payer
		# increase the balance by the payers share
		adjBalance(payer, amount/len(payers))


	for loaner in loaners:
		# print loaner
		# decrease the balance by the loaners share
		adjBalance(loaner, -amount/len(loaners))



# print the results
print "\n"
for key,value in balance.iteritems():

	print "%s\t%s" % (key, value*options.multiplicator) 

print "\nChecksum: %s" % sum(balance.itervalues())

# if fancy output was requested
if options.fancy:
	fancy(balance)